export const SET_USERID = "SET_USERID";

//Action Creator
export const setUserID = (userID) => ({
  type: SET_USERID,
  payload: userID,
});
