import { SET_USERID } from "../actions/userActions";

const userReducer = (state = { auth: { userId: null } }, action) => {
  switch (action.type) {
    case SET_USERID:
      return {
        ...state,
        auth: {
          userId: action.payload,
        },
      };
    default:
      return { ...state };
  }
};

export default userReducer;
