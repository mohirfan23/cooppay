import getConfig from "next/config";
import nc from "next-connect";
import cors from "cors";

const { publicRuntimeConfig } = getConfig();

const handler = nc()
  // use connect based middleware
  .use(cors())
  .post(async (req, res) => {
    //let userID = req.body.userID;
    let pathUrl = `http://localhost/testing-api/`;

    const config = {
      method: "GET",
      //   headers: {
      //     Authorization: "c88ebf0bf801ae7b36bd820cb805dbd6c42fc5d5",
      //   },
    };

    const response = await fetch(pathUrl, config);
    res.json(response);
  });

export default handler;

// export default async function handler(req, res) {

//   if (req.method === "POST") {
//     try {
//       const data = await axios.get(`${baseUrl}/${userID}`, {
//         headers: {
//           Authorization: "c88ebf0bf801ae7b36bd820cb805dbd6c42fc5d5",
//         },
//       });

//       res.status(200).json(data);
//     } catch (error) {
//       console.error(error);
//       return res.status(error.status || 500).end(error.message);
//     }

//     //let data = await fetch(`${baseUrl}/${userID}`, requestOptions);
//     //res.status(200).json(`${baseUrl}/${userID}`);
//   } else {
//     // Handle any other HTTP method
//   }
// }
