import { useEffect } from 'react'
import type { NextPage } from 'next';
import Link from 'next/link';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { userService } from '../services';
import { useRouter } from 'next/router';

import AuthLayout from "../components/AuthLayout";

interface IFormInputs {
  userId: string
  password: string,
  apiError: string
}

const schema = yup.object({
  userId: yup.string().required("User ID harus terisi"),
  password: yup.string().required("Password harus terisi"),
}).required();

const Login: NextPage = () => {

  const router = useRouter()

  useEffect(() => {
    // redirect to home if already logged in
    if (userService.userValue) {
      router.push('/');
    }
  }, []);

  const { register, handleSubmit, setError, formState: { errors } } = useForm<IFormInputs>({
    resolver: yupResolver(schema)
  });

  const onSubmit = (data: IFormInputs) => {
    const { userId, password } = data
    return userService.login(userId, password)
    .then((data) => {
      
      router.push('/');
    })
    .catch(error => {
        setError('apiError', { message: error });
    });
  };

  return (
    <AuthLayout>
      <section className="ftco-section">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-7 col-lg-5">
                <div className="login-wrap p-4 p-md-5">

                  {errors.apiError && <div className="alert alert-danger mt-3 mb-3">{errors.apiError?.message}</div>}
                  
                  <div className="icon d-flex align-items-center justify-content-center">
                    <span className="fa fa-user-o"></span>
                  </div>
                  <h3 className="text-center mb-4">Sign In</h3>
                  <form className="login-form" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-group">
                      <input type="text" {...register("userId")} className="form-control rounded-left" placeholder="User ID" />
                      <p className="text-danger font-weight-danger">{errors.userId?.message}</p>
                    </div>
                    <div className="form-group">
                      <input type="password" {...register("password")} className="form-control rounded-left" placeholder="Kata Sandi" />
                      <p className="text-danger font-weight-danger">{errors.password?.message}</p>
                    </div>
                    <div className="form-group">
                      <button type="submit" className="form-control btn btn-primary rounded submit px-3">Login</button>
                    </div>
                    <div className="form-group d-md-flex">
                      <div className="w-50"></div>
                      <div className="w-50 text-md-right">
                        <Link href="/forgot">Forgot Password</Link>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
    </AuthLayout>
  )
}

export default Login
