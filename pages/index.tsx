import MainLayout from '../components/MainLayout'
import withPrivateRoute from '../components/withPrivateRoute';

interface HomeProps {
    user: string
}

export default function Home(props:HomeProps){
    const { user } = props;

    return (
        <MainLayout>
           <div className="orders">
                <div className="row">
                    <div className="col-xl-4">
                        <div className="card">
                           
                            <div className="card-body">
                                <form className="py-3 px-3">
                                    <div className="form-group">
                                        <label className="control-label mr-2 font-weight-bold">MSISDN</label>
                                        <input id="cc-payment" name="cc-payment" type="text" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label mr-2 font-weight-bold">NILAI</label>
                                        <input id="cc-payment" disabled name="cc-payment" type="text" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label className="control-label mr-2 font-weight-bold">PRODUK</label>
                                        <input id="cc-payment" disabled name="cc-payment" type="text" className="form-control" />
                                    </div>
                                    <div className="form-group d-flex flex-row justify-content-between">
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" />
                                            <label className="form-check-label" htmlFor="flexRadioDefault2">
                                                Cetak Resi
                                            </label>
                                        </div>
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
                                            <label className="form-check-label" htmlFor="flexRadioDefault2">
                                                Tidak Cetak
                                            </label>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn btn-primary btn-block">Bayar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-8">
                        <div className="card">
                            <div className="card-title p-3">
                               <h4 className="font-weight-bold">Resi</h4>
                            </div>
                            <hr className="mt-0"/>
                            <div className="card-body" style={{height:'200px'}}>
                               
                            </div>
                        </div> 
                    </div>  
                </div>
            </div>
        </MainLayout>
    )
}


export async function getServerSideProps({req}) {
    const { token } = req.cookies
    if(!token){
        return {
            redirect : {
                destination: '/login',
                permanent: false
            }
        }
    }

    const user = Buffer.from(token, "base64").toString("ascii");
    return {
        props: {
            user
        }
    }
}
// export default withPrivateRoute(Home)
