import type { NextPage } from 'next';
// import Link from 'next/link';
import AuthLayout from "../components/AuthLayout";

const Forgot: NextPage = () => {
  return (
    <AuthLayout>
      <section className="ftco-section">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-7 col-lg-5">
                <div className="login-wrap p-4 p-md-5">
                
                  <h3 className="text-center mb-4">Forgot Password</h3>
                  <form action="#" className="login-form">
                    <div className="form-group">
                      <input type="text" className="form-control rounded-left" placeholder="UserId" required />
                    </div>
                    <div className="form-group">
                      <button type="submit" className="form-control btn btn-primary rounded submit px-3">Kirim</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
    </AuthLayout>
  )
}

export default Forgot
