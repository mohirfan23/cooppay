const path = require("path");

/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  serverRuntimeConfig: {
    secret: "rlinkkopnusjayajay",
  },
  publicRuntimeConfig: {
    apiUrl:
      process.env.NODE_ENV === "development"
        ? "http://10.2.2.183:8181/boc" // development api
        : "http://10.2.2.183:8181/boc", // production api
  },
  // async rewrites() {
  //   return [
  //     {
  //       source: "/api/menu",
  //       destination: "http://10.2.2.183:8181/boc/",
  //     },
  //   ];
  // },
};
