import Head from 'next/head';
import { ReactNode } from 'react';

interface LayoutProps {
    children: ReactNode
}

const AuthLayout = (props:LayoutProps) => {
    const { children } = props;
    return (
        <>
            <Head>
                <title>Login 01</title>

                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

                <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet" />
                
                <link rel="stylesheet" href="/static/login/css/style.css" />
            </Head>
            
            {children}
        </>
    )
}

export default AuthLayout;
