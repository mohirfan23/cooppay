import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt, faClock } from '@fortawesome/free-solid-svg-icons'

const InfoSaldo = () => {
    return (
        <div className="breadcrumbs">
            <div className="breadcrumbs-inner p-3">
                <div className="row m-0">
                    <div className="col-sm-4 p-0">
                        <div className="page-header float-left">
                            <div className="page-title">
                                <h1>SALDO DEPOSIT : <span className="text-danger font-weight-bold">Rp. 6.000.000</span></h1>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-8 p-0">
                        <div className="page-header float-right">
                            <div className="page-title d-flex flex-column">
                                <div className="d-flex flex-row justify-content-end align-items-center">
                                    <h5 className="font-weight-bold">12-12-2021</h5>
                                    <FontAwesomeIcon icon={faCalendarAlt} size="sm" className="ml-2" />
                                </div>
                                <div className="d-flex flex-row justify-content-end align-items-center">
                                    <p className="p-0 m-0">08:20:00</p>
                                    <FontAwesomeIcon icon={faClock} size="sm" className="ml-2" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default InfoSaldo;