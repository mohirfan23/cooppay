import React from 'react'
import dynamic from 'next/dynamic'
import Link from 'next/link';

import { userService } from "../services";

import { ReactNode } from 'react';

const InfoSaldo = dynamic(() => import('../components/InfoSaldo'), { loading: () => <p>Loading ...</p> })

const SideMenuTest = dynamic(() => import('../components/SideMenu'), { loading: () => <p>Loading ...</p> })


interface LayoutProps {
    children: ReactNode
}

function logout() {
    userService.logout();
}

const MainLayout = (props:LayoutProps) => {
    const { children } = props;

    return (
        <>
           
            <SideMenuTest />

            <div id="right-panel" className="right-panel">
                <header id="header" className="header">
                    <div className="top-left">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="./"><img src="/static/images/logo.png" alt="Logo" /></a>
                            <a className="navbar-brand hidden" href="./"><img src="/static/images/logo2.png" alt="Logo" /></a>
                            <a id="menuToggle" className="menutoggle"><i className="fa fa-bars"></i></a>
                        </div>
                    </div>
                    <div className="top-right">
                        <div className="header-menu">
                            <div className="header-left d-flex justify-content-center align-items-center">
                                <p className="font-weight-bold m-0" style={{fontSize:'14px', color:'black'}}>Petugas : Agung Wijaya</p>
                            </div>
                            <div className="user-area dropdown float-right">
                                <a href="#" className="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img className="user-avatar rounded-circle" src="/static/images/admin.jpg" alt="User Avatar" />
                                </a>

                                <div className="user-menu dropdown-menu">
                                    <a className="nav-link" href="#"><i className="fa fa- user"></i>My Profile</a>
                                    <a className="nav-link" href="#"><i className="fa fa- user"></i>Notifications <span className="count">13</span></a>
                                    <a className="nav-link" href="#"><i className="fa fa -cog"></i>Settings</a>
                                    <a className="nav-link" onClick={logout} href="#"><i className="fa fa-power -off"></i>Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <InfoSaldo />

                <div className="content">
                    <div className="animated fadeIn">
                        {children}
                    </div>
                </div>
                
                <div className="clearfix"></div>

                <footer className="site-footer">
                    <div className="footer-inner bg-white">
                        <div className="row">
                            <div className="col-sm-6">
                                Copyright &copy; 2018 Ela Admin
                            </div>
                            <div className="col-sm-6 text-right">
                                Designed by <a href="https://colorlib.com">Colorlib</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </>
    )
}

export default MainLayout;
