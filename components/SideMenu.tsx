import { useEffect, useState } from "react"
import { menuServices } from '../services'
import { NextPage } from 'next'
import Cookies from 'js-cookie'

import Skeleton from 'react-loading-skeleton';

const SideMenu:NextPage = () => {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState(null)

    useEffect(() => {
        const token = Cookies.get('token')

        if(token){
            const decode = atob(data);
            let pecah = decode.slice(1,-1).split("|")
        
            // menuServices.getMenu(pecah[9]).then((data) => {
            //     console.log(data)
            // })
            // .catch((error) => {
            //     console.log(error)
            // })
        }
        
    },[])

    const loadingComponent = () => {
        return (
            <>
                <li className="menu-item-has-children">
                    <Skeleton height={25} className="mb-2" width="100%" count={2} />
                </li>
                <li className="menu-item-has-children">
                    <Skeleton height={25} className="mb-2" width="100%" count={2} />
                </li>
            </>
        )
    }

    return (
        <aside id="left-panel" className="left-panel">
            <nav className="navbar navbar-expand-sm navbar-default">
                <div id="main-menu" className="main-menu collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                        <li className="menu-title">Main Menu</li>
                        {/* {loadingComponent()} */}
                        <li className="menu-item-has-children dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="menu-icon fa fa-th"></i>Pembayaran</a>
                            <ul className="sub-menu children dropdown-menu">
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-basic.html">Telkom</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Telkomsel</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Telkom TV</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PDAM</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Zakat & Infaq</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Leasing</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PLN Postpaid</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">TV Postpaid</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Pajak</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PLN Nontaglis</a></li>
                            </ul>
                        </li>
                        <li className="menu-item-has-children dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            <i className="menu-icon fa fa-th"></i>Pembelian</a>
                            <ul className="sub-menu children dropdown-menu">
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-basic.html">Telkom</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Telkomsel</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Telkom TV</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PDAM</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Zakat & Infaq</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Leasing</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PLN Postpaid</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">TV Postpaid</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Pajak</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PLN Nontaglis</a></li>
                            </ul>
                        </li>
                        <li className="menu-item-has-children dropdown">
                            <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            <i className="menu-icon fa fa-th"></i>Laporan</a>
                            <ul className="sub-menu children dropdown-menu">
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-basic.html">Telkom</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Telkomsel</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Telkom TV</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PDAM</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Zakat & Infaq</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Leasing</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PLN Postpaid</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">TV Postpaid</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">Pajak</a></li>
                                <li><i className="menu-icon fa fa-th"></i><a href="forms-advanced.html">PLN Nontaglis</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </aside>
    )
}


export default SideMenu
