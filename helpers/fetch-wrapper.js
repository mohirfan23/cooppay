import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

import { userService } from "../services";

const get = (url) => {
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "c88ebf0bf801ae7b36bd820cb805dbd6c42fc5d5",
    },
  };

  return fetch(url, requestOptions).then(handleResponse);
};

const post = (url, body) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json", ...authHeader(url) },
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions).then(handleResponse);
};

const authHeader = (url) => {
  // return auth header with jwt if user is logged in and request is to the api url
  const user = userService.userValue;
  const isLoggedIn = user && user.token;

  const isApiUrl = url.startsWith(publicRuntimeConfig.apiUrl);
  if (isLoggedIn && isApiUrl) {
    return { Authorization: `${user.token}` };
  } else {
    return {};
  }
};

const handleResponse = (response) => {
  return response.text().then((text) => {
    const data = text.split("|");

    if (!response.ok) {
      if ([401, 403].includes(response.status) && userService.userValue) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        userService.logout();
      }

      const error = data[1] || response.statusText;
      return Promise.reject(error);
    }

    if (data[0] != "00") {
      return Promise.reject(data[1]);
    }

    return text;
  });
};

export const fetchWrapper = {
  get,
  post,
};
