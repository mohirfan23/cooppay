import getConfig from "next/config";

import { fetchWrapper } from "../helpers";

const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}/data/get/allmenubyuserid`;

const getMenu = (userId) => {
  return fetchWrapper.get(`${baseUrl}/${userId}`).then((data) => {
    return data;
  });
};

export const menuServices = {
  getMenu,
};
