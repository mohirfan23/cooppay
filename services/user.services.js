import { BehaviorSubject } from "rxjs";
import getConfig from "next/config";
import Router from "next/router";
import Cookies from "js-cookie";

import { fetchWrapper } from "../helpers";

const { publicRuntimeConfig } = getConfig();
const baseUrl = `${publicRuntimeConfig.apiUrl}/loginbox`;
const userSubject = new BehaviorSubject(
  process.browser && JSON.parse(localStorage.getItem("user"))
);

const login = (userId, password) => {
  return fetchWrapper.post(`${baseUrl}`, { userId, password }).then((user) => {
    const tokenBase64 = btoa(user + `|${userId}`, "base64");

    userSubject.next(tokenBase64);
    // localStorage.setItem("user", JSON.stringify());
    Cookies.set("token", tokenBase64, { expires: 1 });

    return user;
  });
};

const logout = () => {
  Cookies.remove("token");
  userSubject.next(null);
  Router.push("/login");
};

export const userService = {
  user: userSubject.asObservable(),
  get userValue() {
    return userSubject.value;
  },
  login,
  logout,
};
